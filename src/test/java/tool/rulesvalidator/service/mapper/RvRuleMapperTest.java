package tool.rulesvalidator.service.mapper;

import static org.assertj.core.api.Assertions.assertThat;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class RvRuleMapperTest {
    private RvRuleMapper rvRuleMapper;

    @BeforeEach
    public void setUp() {
        rvRuleMapper = new RvRuleMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        Assertions.assertThat(rvRuleMapper.fromId(id).getId()).isEqualTo(id);
        Assertions.assertThat(rvRuleMapper.fromId(null)).isNull();
    }
}
