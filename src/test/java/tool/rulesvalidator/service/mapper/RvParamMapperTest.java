package tool.rulesvalidator.service.mapper;

import static org.assertj.core.api.Assertions.assertThat;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class RvParamMapperTest {
    private RvParamMapper rvParamMapper;

    @BeforeEach
    public void setUp() {
        rvParamMapper = new RvParamMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        Assertions.assertThat(rvParamMapper.fromId(id).getId()).isEqualTo(id);
        Assertions.assertThat(rvParamMapper.fromId(null)).isNull();
    }
}
