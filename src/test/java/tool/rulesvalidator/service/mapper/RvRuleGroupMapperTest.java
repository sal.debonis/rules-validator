package tool.rulesvalidator.service.mapper;

import static org.assertj.core.api.Assertions.assertThat;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class RvRuleGroupMapperTest {
    private RvRuleGroupMapper rvRuleGroupMapper;

    @BeforeEach
    public void setUp() {
        rvRuleGroupMapper = new RvRuleGroupMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        Assertions.assertThat(rvRuleGroupMapper.fromId(id).getId()).isEqualTo(id);
        Assertions.assertThat(rvRuleGroupMapper.fromId(null)).isNull();
    }
}
