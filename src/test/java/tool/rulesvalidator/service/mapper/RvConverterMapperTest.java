package tool.rulesvalidator.service.mapper;

import static org.assertj.core.api.Assertions.assertThat;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class RvConverterMapperTest {
    private RvConverterMapper rvConverterMapper;

    @BeforeEach
    public void setUp() {
        rvConverterMapper = new RvConverterMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        Assertions.assertThat(rvConverterMapper.fromId(id).getId()).isEqualTo(id);
        Assertions.assertThat(rvConverterMapper.fromId(null)).isNull();
    }
}
