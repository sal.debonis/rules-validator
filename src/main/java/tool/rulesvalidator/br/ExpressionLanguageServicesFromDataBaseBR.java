package tool.rulesvalidator.br;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import tool.rulesvalidator.br.impl.DefaultExpressionLanguageServicesBR;
import tool.rulesvalidator.service.RvRuleService;
import tool.rulesvalidator.service.dto.RvRuleDTO;

@Component
public class ExpressionLanguageServicesFromDataBaseBR extends DefaultExpressionLanguageServicesBR {
    private Cache<RvRuleDTO> rules = new Cache<>(5 * 60 * 1000); //Cache a 5 minuti

    @Autowired
    private RvRuleService rvRuleService;

    @Override
    public RvRuleDTO rule(String name) {
        RvRuleDTO rule = this.rules.getCache(name);
        if (rule == null) {
            rule = rvRuleService.findOne(name).orElse(null);
            this.rules.setCache(rule, name);
        }
        return rule;
    }
}
