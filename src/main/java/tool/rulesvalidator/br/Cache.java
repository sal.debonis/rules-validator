package tool.rulesvalidator.br;

import java.util.HashMap;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;

public class Cache<T> {
    private long timeLimit;
    private Map<CacheKey, CacheTime<T>> cache = new HashMap<>();

    public Cache(long timeLimit) {
        this.timeLimit = timeLimit;
    }

    public T getCache(Object... key) {
        T el = null;
        CacheTime<T> tCacheTime = cache.get(new CacheKey(key));
        if (tCacheTime != null) {
            el = tCacheTime.getCached();
            long now = System.currentTimeMillis();
            if (now >= (tCacheTime.getTime() + timeLimit)) {
                cache.remove(new CacheKey(key));
                el = null;
            }
        }
        return el;
    }

    public T setCache(T el, Object... key) {
        CacheTime<T> tCacheTime = new CacheTime<>(System.currentTimeMillis(), el);
        cache.put(new CacheKey(key), tCacheTime);
        return el;
    }
}

@Data
class CacheKey {
    private String key;

    public CacheKey(Object... keys) {
        key = StringUtils.join(keys);
    }
}

@Data
@NoArgsConstructor
@AllArgsConstructor
class CacheTime<T> {
    private Long time;
    private T cached;
}
