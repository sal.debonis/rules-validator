package tool.rulesvalidator.service;

import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import tool.rulesvalidator.domain.RvRuleGroup;
import tool.rulesvalidator.service.dto.RvRuleGroupDTO;

/**
 * Service Interface for managing {@link RvRuleGroup}.
 */
public interface RvRuleGroupService {
    /**
     * Save a rvRuleGroup.
     *
     * @param rvRuleGroupDTO the entity to save.
     * @return the persisted entity.
     */
    RvRuleGroupDTO save(RvRuleGroupDTO rvRuleGroupDTO);

    /**
     * Get all the rvRuleGroups.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<RvRuleGroupDTO> findAll(Pageable pageable);

    /**
     * Get the "id" rvRuleGroup.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<RvRuleGroupDTO> findOne(Long id);

    /**
     * Delete the "id" rvRuleGroup.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
