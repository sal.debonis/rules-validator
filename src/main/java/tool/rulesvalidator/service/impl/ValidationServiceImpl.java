package tool.rulesvalidator.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import tool.rulesvalidator.br.ApplyRuleBR;
import tool.rulesvalidator.domain.RvOperator;
import tool.rulesvalidator.domain.enumeration.EFormat;
import tool.rulesvalidator.service.RvRuleService;
import tool.rulesvalidator.service.ValidationService;
import tool.rulesvalidator.service.dto.*;
import tool.rulesvalidator.service.errors.ValidationException;
import tool.rulesvalidator.web.rest.errors.BadRequestAlertException;

/**
 * Service Implementation for managing {@link RvOperator}.
 */
@Service
@Transactional
public class ValidationServiceImpl implements ValidationService {
    private final Logger log = LoggerFactory.getLogger(ValidationServiceImpl.class);

    @Autowired
    private ObjectMapper objectMapper;

    private XmlMapper xmlMapper = new XmlMapper();

    @Autowired
    private RvRuleService rvRuleService;

    @Autowired
    private ApplyRuleBR applyRuleBR;

    @Override
    @Transactional(readOnly = true)
    public RvValidationResultDTO validate(EFormat format, RvValidationRequestDTO validationRequestDTO) throws ValidationException {
        RvValidationResultDTO resultDTO = new RvValidationResultDTO();
        try {
            JsonNode model = null;
            if (EFormat.XML.equals(format)) {
                model = xmlMapper.readTree(validationRequestDTO.getModel());
            } else {
                model = objectMapper.readTree(validationRequestDTO.getModel());
            }
            RvRuleDTO ruleDTO = rvRuleService.findOne(validationRequestDTO.getRuleCode()).orElse(null);
            if (ruleDTO == null) {
                throw new ValidationException("Rule not found: " + validationRequestDTO.getRuleCode());
            }
            List<RvValidationResultDetailDTO> detailDTOS = applyRuleBR.applyRule(ruleDTO, model);
            if (!CollectionUtils.isEmpty(detailDTOS)) {
                resultDTO.setValid(false);
                resultDTO.getDetails().addAll(detailDTOS);
            }
        } catch (JsonProcessingException e) {
            throw new BadRequestAlertException("Invalid format for Model", "", "invalidFormat");
        }
        return resultDTO;
    }

    @Override
    @Transactional(readOnly = true)
    public String makeXml(List<Long> ids) {
        List<RvRuleDTO> all = rvRuleService.findAll(ids);
        return toXml(all);
    }

    public String toXml(List<RvRuleDTO> rvRuleDTOS) {
        //Header
        StringBuilder sb = new StringBuilder(
            "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
            "<beans xmlns=\"http://www.springframework.org/schema/beans\"\n" +
            "       xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:context=\"http://www.springframework.org/schema/context\"\n" +
            "       xmlns:aop=\"http://www.springframework.org/schema/aop\"\n" +
            "       xsi:schemaLocation=\"http://www.springframework.org/schema/beans\n" +
            "    http://www.springframework.org/schema/beans/spring-beans-3.0.xsd\n" +
            "    http://www.springframework.org/schema/context\n" +
            "    http://www.springframework.org/schema/context/spring-context-3.0.xsd\n" +
            "    http://www.springframework.org/schema/aop\n" +
            "    http://www.springframework.org/schema/aop/spring-aop-3.0.xsd\n" +
            "    \"\n" +
            "       default-autowire=\"byType\">\n"
        );
        Map<String, RvRuleGroupDTO> groups = new HashMap<>();
        //Body
        sb.append("\n    <!-- RULES -->\n");
        for (RvRuleDTO rule : rvRuleDTOS) {
            sb.append("    <bean id=\"").append(rule.getRuleCode()).append("\" class=\"tool.rulesvalidator.service.dto.RvRuleDTO\">\n");
            sb.append("        <property name=\"ruleCode\" value=\"").append(rule.getRuleCode()).append("\"/>\n");
            if (rule.getDescription() != null) sb
                .append("        <property name=\"description\" value=\"")
                .append(rule.getDescription())
                .append("\"/>\n");
            if (rule.getLevel() != null) sb.append("        <property name=\"level\" value=\"").append(rule.getLevel()).append("\"/>\n");
            if (rule.getMode() != null) sb.append("        <property name=\"mode\" value=\"").append(rule.getMode()).append("\"/>\n");
            if (rule.getGroup() != null) {
                groups.put(rule.getGroup().getRuleGroupName(), rule.getGroup());
                sb.append("        <property name=\"group\" ref=\"").append(rule.getGroup().getRuleGroupName()).append("\"/>\n");
            }
            if (rule.getOperator() != null) {
                sb.append("        <property name=\"operator\">\n");
                sb.append("            <bean class=\"tool.rulesvalidator.service.dto.RvOperatorDTO\">\n");
                sb
                    .append("                <property name=\"operatorCode\" value=\"")
                    .append(rule.getOperator().getOperatorCode())
                    .append("\"/>\n");
                sb.append("                <property name=\"beanName\" value=\"").append(rule.getOperator().getBeanName()).append("\"/>\n");
                sb.append("            </bean>\n");
                sb.append("        </property>\n");
            }
            if (!CollectionUtils.isEmpty(rule.getRvParams())) {
                sb.append("        <property name=\"rvParams\"><list>\n");
                for (RvParamDTO param : rule.getRvParams()) {
                    sb.append("                <bean class=\"tool.rulesvalidator.service.dto.RvParamDTO\">\n");
                    sb.append("                    <property name=\"value\" value=\"").append(param.getValue()).append("\"/>\n");
                    if (!CollectionUtils.isEmpty(param.getRvConverters())) {
                        sb.append("        <property name=\"rvConverters\"><list>\n");
                        for (RvConverterDTO converter : param.getRvConverters()) {
                            sb.append("                <bean class=\"tool.rulesvalidator.service.dto.RvConverterDTO\">\n");
                            sb
                                .append("                    <property name=\"converterCode\" value=\"")
                                .append(converter.getConverterCode())
                                .append("\"/>\n");
                            sb
                                .append("                    <property name=\"description\" value=\"")
                                .append(converter.getDescription())
                                .append("\"/>\n");
                            sb
                                .append("                    <property name=\"beanName\" value=\"")
                                .append(converter.getBeanName())
                                .append("\"/>\n");
                            sb.append("                </bean>\n");
                        }
                        sb.append("        </list></property>\n");
                    }
                    sb.append("                </bean>\n");
                }
                sb.append("        </list></property>\n");
            }
            sb.append("    </bean>\n");
        }

        sb.append("\n    <!-- GROUPS -->\n");
        for (RvRuleGroupDTO group : groups.values()) {
            sb
                .append("    <bean id=\"")
                .append(group.getRuleGroupName())
                .append("\" class=\"tool.rulesvalidator.service.dto.RvRuleGroupDTO\">\n");
            sb.append("        <property name=\"ruleGroupName\" value=\"").append(group.getRuleGroupName()).append("\"/>\n");
            sb.append("    </bean>\n");
        }

        //Footer
        sb.append(
            "\n    <context:annotation-config />\n" + "    <context:component-scan base-package=\"tool.rulesvalidator\" />\n" + "</beans>"
        );
        return sb.toString();
    }
}
