package tool.rulesvalidator.service.impl;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tool.rulesvalidator.domain.RvRule;
import tool.rulesvalidator.repository.RvRuleRepository;
import tool.rulesvalidator.service.RvRuleService;
import tool.rulesvalidator.service.dto.RvRuleDTO;
import tool.rulesvalidator.service.mapper.RvRuleMapper;

/**
 * Service Implementation for managing {@link RvRule}.
 */
@Service
@Transactional
public class RvRuleServiceImpl implements RvRuleService {
    private final Logger log = LoggerFactory.getLogger(RvRuleServiceImpl.class);

    private final RvRuleRepository rvRuleRepository;

    private final RvRuleMapper rvRuleMapper;

    public RvRuleServiceImpl(RvRuleRepository rvRuleRepository, RvRuleMapper rvRuleMapper) {
        this.rvRuleRepository = rvRuleRepository;
        this.rvRuleMapper = rvRuleMapper;
    }

    @Override
    public RvRuleDTO save(RvRuleDTO rvRuleDTO) {
        log.debug("Request to save RvRule : {}", rvRuleDTO);
        RvRule rvRule = rvRuleMapper.toEntity(rvRuleDTO);
        rvRule = rvRuleRepository.save(rvRule);
        return rvRuleMapper.toDto(rvRule);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<RvRuleDTO> findAll(Pageable pageable) {
        log.debug("Request to get all RvRules");
        return rvRuleRepository.findAll(pageable).map(rvRuleMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public List<RvRuleDTO> findAll(Long groupId) {
        log.debug("Request to get all RvRules");
        return rvRuleRepository.findByGroupId(groupId).stream().map(rvRuleMapper::toDto).collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public List<RvRuleDTO> findAll(List<Long> ids) {
        log.debug("Request to get all RvRules");
        return rvRuleRepository.findByIdIn(ids).stream().map(rvRuleMapper::toDto).collect(Collectors.toList());
    }

    public Page<RvRuleDTO> findAllWithEagerRelationships(Pageable pageable) {
        return rvRuleRepository.findAllWithEagerRelationships(pageable).map(rvRuleMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<RvRuleDTO> findOne(Long id) {
        log.debug("Request to get RvRule : {}", id);
        return rvRuleRepository.findOneWithEagerRelationships(id).map(rvRuleMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<RvRuleDTO> findOne(String ruleCode) {
        log.debug("Request to get RvRule : {}", ruleCode);
        return rvRuleRepository.findOneWithEagerRelationships(ruleCode).map(rvRuleMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete RvRule : {}", id);
        rvRuleRepository.deleteById(id);
    }
}
