package tool.rulesvalidator.service.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import tool.rulesvalidator.domain.RvRule;
import tool.rulesvalidator.service.dto.RvRuleDTO;

/**
 * Mapper for the entity {@link RvRule} and its DTO {@link RvRuleDTO}.
 */
@Mapper(componentModel = "spring", uses = { RvEnumMapper.class, RvRuleGroupMapper.class, RvOperatorMapper.class, RvParamMapper.class })
public interface RvRuleMapper extends EntityMapper<RvRuleDTO, RvRule> {
    @Mapping(source = "group", target = "group")
    @Mapping(source = "operator", target = "operator")
    RvRuleDTO toDto(RvRule rvRule);

    @Mapping(source = "group.id", target = "group")
    @Mapping(source = "operator.id", target = "operator")
    @Mapping(target = "removeRvParam", ignore = true)
    RvRule toEntity(RvRuleDTO rvRuleDTO);

    default RvRule fromId(Long id) {
        if (id == null) {
            return null;
        }
        RvRule rvRule = new RvRule();
        rvRule.setId(id);
        return rvRule;
    }

    @Mappings({ @Mapping(source = "id", target = "id", ignore = true) })
    RvRuleDTO clone(RvRuleDTO dto);
}
