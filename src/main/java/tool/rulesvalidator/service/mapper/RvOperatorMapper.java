package tool.rulesvalidator.service.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import tool.rulesvalidator.domain.RvOperator;
import tool.rulesvalidator.service.dto.RvOperatorDTO;

/**
 * Mapper for the entity {@link RvOperator} and its DTO {@link RvOperatorDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface RvOperatorMapper extends EntityMapper<RvOperatorDTO, RvOperator> {
    @Mapping(target = "params", ignore = true)
    @Mapping(target = "removeParams", ignore = true)
    RvOperator toEntity(RvOperatorDTO rvOperatorDTO);

    default RvOperator fromId(Long id) {
        if (id == null) {
            return null;
        }
        RvOperator rvOperator = new RvOperator();
        rvOperator.setId(id);
        return rvOperator;
    }

    @Mappings({ @Mapping(source = "id", target = "id", ignore = true) })
    RvOperatorDTO clone(RvOperatorDTO dto);
}
