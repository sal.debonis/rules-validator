package tool.rulesvalidator.service.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import tool.rulesvalidator.domain.RvOperatorParam;
import tool.rulesvalidator.service.dto.RvOperatorParamDTO;

/**
 * Mapper for the entity {@link RvOperatorParam} and its DTO {@link RvOperatorParamDTO}.
 */
@Mapper(componentModel = "spring", uses = { RvEnumMapper.class, RvOperatorMapper.class })
public interface RvOperatorParamMapper extends EntityMapper<RvOperatorParamDTO, RvOperatorParam> {
    @Mapping(source = "operator.id", target = "operatorId")
    RvOperatorParamDTO toDto(RvOperatorParam rvOperatorParam);

    @Mapping(source = "operatorId", target = "operator")
    RvOperatorParam toEntity(RvOperatorParamDTO rvOperatorParamDTO);

    default RvOperatorParam fromId(Long id) {
        if (id == null) {
            return null;
        }
        RvOperatorParam rvOperatorParam = new RvOperatorParam();
        rvOperatorParam.setId(id);
        return rvOperatorParam;
    }
}
