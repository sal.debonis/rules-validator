package tool.rulesvalidator.service.mapper;

import org.mapstruct.Mapper;
import tool.rulesvalidator.domain.enumeration.RvParamType;
import tool.rulesvalidator.domain.enumeration.RvRuleLevel;
import tool.rulesvalidator.domain.enumeration.RvRuleMode;
import tool.rulesvalidator.service.enums.RvParamTypeDTO;
import tool.rulesvalidator.service.enums.RvRuleLevelDTO;
import tool.rulesvalidator.service.enums.RvRuleModeDTO;

@Mapper(componentModel = "spring", uses = {})
public interface RvEnumMapper {
    default RvRuleLevelDTO convert(RvRuleLevel param) {
        return RvRuleLevelDTO.valueOf(param.name());
    }

    default RvRuleLevel convert(RvRuleLevelDTO param) {
        return RvRuleLevel.valueOf(param.name());
    }

    default RvRuleModeDTO convert(RvRuleMode param) {
        return RvRuleModeDTO.valueOf(param.name());
    }

    default RvRuleMode convert(RvRuleModeDTO param) {
        return RvRuleMode.valueOf(param.name());
    }

    default RvParamTypeDTO convert(RvParamType param) {
        return RvParamTypeDTO.valueOf(param.name());
    }

    default RvParamType convert(RvParamTypeDTO param) {
        return RvParamType.valueOf(param.name());
    }
}
