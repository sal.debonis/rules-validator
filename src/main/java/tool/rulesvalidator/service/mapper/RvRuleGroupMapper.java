package tool.rulesvalidator.service.mapper;

import org.mapstruct.Mapper;
import tool.rulesvalidator.domain.RvRuleGroup;
import tool.rulesvalidator.service.dto.RvRuleGroupDTO;

/**
 * Mapper for the entity {@link RvRuleGroup} and its DTO {@link RvRuleGroupDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface RvRuleGroupMapper extends EntityMapper<RvRuleGroupDTO, RvRuleGroup> {
    default RvRuleGroup fromId(Long id) {
        if (id == null) {
            return null;
        }
        RvRuleGroup rvRuleGroup = new RvRuleGroup();
        rvRuleGroup.setId(id);
        return rvRuleGroup;
    }
}
