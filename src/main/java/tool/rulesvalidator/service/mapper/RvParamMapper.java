package tool.rulesvalidator.service.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import tool.rulesvalidator.domain.RvParam;
import tool.rulesvalidator.service.dto.RvParamDTO;

/**
 * Mapper for the entity {@link RvParam} and its DTO {@link RvParamDTO}.
 */
@Mapper(componentModel = "spring", uses = { RvConverterMapper.class })
public interface RvParamMapper extends EntityMapper<RvParamDTO, RvParam> {
    @Mapping(target = "removeRvConverter", ignore = true)
    @Mapping(target = "rvRules", ignore = true)
    @Mapping(target = "removeRvRule", ignore = true)
    RvParam toEntity(RvParamDTO rvParamDTO);

    default RvParam fromId(Long id) {
        if (id == null) {
            return null;
        }
        RvParam rvParam = new RvParam();
        rvParam.setId(id);
        return rvParam;
    }

    @Mappings({ @Mapping(source = "id", target = "id", ignore = true) })
    RvParamDTO clone(RvParamDTO dto);
}
