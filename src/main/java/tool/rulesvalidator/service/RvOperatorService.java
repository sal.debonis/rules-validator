package tool.rulesvalidator.service;

import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import tool.rulesvalidator.domain.RvOperator;
import tool.rulesvalidator.service.dto.RvOperatorDTO;

/**
 * Service Interface for managing {@link RvOperator}.
 */
public interface RvOperatorService {
    /**
     * Save a rvOperator.
     *
     * @param rvOperatorDTO the entity to save.
     * @return the persisted entity.
     */
    RvOperatorDTO save(RvOperatorDTO rvOperatorDTO);

    /**
     * Get all the rvOperators.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<RvOperatorDTO> findAll(Pageable pageable);

    /**
     * Get the "id" rvOperator.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<RvOperatorDTO> findOne(Long id);

    /**
     * Delete the "id" rvOperator.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
