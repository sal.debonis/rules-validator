package tool.rulesvalidator.service;

import java.util.List;
import tool.rulesvalidator.domain.enumeration.EFormat;
import tool.rulesvalidator.service.dto.RvValidationRequestDTO;
import tool.rulesvalidator.service.dto.RvValidationResultDTO;
import tool.rulesvalidator.service.errors.ValidationException;

/**
 * Service Interface for validation.
 */
public interface ValidationService {
    RvValidationResultDTO validate(EFormat format, RvValidationRequestDTO validationRequestDTO) throws ValidationException;
    String makeXml(List<Long> ids);
}
