package tool.rulesvalidator.domain.enumeration;

/**
 * The RvParamType enumeration.
 */
public enum RvParamType {
    STRING,
    INTEGER,
    DECIMAL,
    DATE,
    RULE,
    FIELD,
}
