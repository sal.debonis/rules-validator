package tool.rulesvalidator.domain.enumeration;

/**
 * The RvRuleMode enumeration.
 */
public enum RvRuleMode {
    FIRST_ERROR,
    ALL_VALIDATION,
}
