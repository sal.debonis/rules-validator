package tool.rulesvalidator.domain.enumeration;

public enum EFormat {
    JSON,
    XML,
}
