package tool.rulesvalidator.domain.enumeration;

/**
 * The RvRuleLevel enumeration.
 */
public enum RvRuleLevel {
    CRITICAL,
    WARNING,
}
