package tool.rulesvalidator.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import tool.rulesvalidator.domain.enumeration.EFormat;
import tool.rulesvalidator.service.ValidationService;
import tool.rulesvalidator.service.dto.RvValidationRequestDTO;
import tool.rulesvalidator.service.dto.RvValidationResultDTO;
import tool.rulesvalidator.service.errors.ValidationException;

@Controller
@RequestMapping("/api")
public class ValidationController {
    @Autowired
    private ValidationService validationService;

    @PutMapping("/validate")
    public ResponseEntity<RvValidationResultDTO> validate(@Valid @RequestBody RvValidationRequestDTO validationRequestDTO)
        throws URISyntaxException, ValidationException {
        RvValidationResultDTO result = validationService.validate(EFormat.JSON, validationRequestDTO);
        return ResponseEntity.ok().body(result);
    }

    @PutMapping("/validate/{format}")
    public ResponseEntity<RvValidationResultDTO> validateFormat(
        @PathVariable("format") EFormat format,
        @Valid @RequestBody RvValidationRequestDTO validationRequestDTO
    )
        throws URISyntaxException, ValidationException {
        RvValidationResultDTO result = validationService.validate(format, validationRequestDTO);
        return ResponseEntity.ok().body(result);
    }

    @PutMapping("/export")
    public ResponseEntity<String> validate(@Valid @RequestBody List<Long> ids) throws URISyntaxException {
        String result = validationService.makeXml(ids);
        return ResponseEntity.ok().body(result);
    }
}
