/**
 * View Models used by Spring MVC REST controllers.
 */
package tool.rulesvalidator.web.rest.vm;
