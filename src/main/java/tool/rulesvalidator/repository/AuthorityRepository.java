package tool.rulesvalidator.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import tool.rulesvalidator.domain.Authority;

/**
 * Spring Data JPA repository for the {@link Authority} entity.
 */
public interface AuthorityRepository extends JpaRepository<Authority, String> {}
