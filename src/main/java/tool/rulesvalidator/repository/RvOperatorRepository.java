package tool.rulesvalidator.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tool.rulesvalidator.domain.RvOperator;

/**
 * Spring Data  repository for the RvOperator entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RvOperatorRepository extends JpaRepository<RvOperator, Long> {}
