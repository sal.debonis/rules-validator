package tool.rulesvalidator.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tool.rulesvalidator.domain.RvRuleGroup;

/**
 * Spring Data  repository for the RvRuleGroup entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RvRuleGroupRepository extends JpaRepository<RvRuleGroup, Long> {}
