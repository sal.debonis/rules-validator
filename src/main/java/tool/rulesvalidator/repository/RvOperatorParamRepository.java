package tool.rulesvalidator.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tool.rulesvalidator.domain.RvOperatorParam;

/**
 * Spring Data  repository for the RvOperatorParam entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RvOperatorParamRepository extends JpaRepository<RvOperatorParam, Long> {}
