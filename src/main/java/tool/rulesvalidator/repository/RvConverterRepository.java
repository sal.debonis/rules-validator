package tool.rulesvalidator.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tool.rulesvalidator.domain.RvConverter;

/**
 * Spring Data  repository for the RvConverter entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RvConverterRepository extends JpaRepository<RvConverter, Long> {}
